package net.decenternet.technicalexam.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.decenternet.technicalexam.TasksApplication;
import net.decenternet.technicalexam.domain.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TaskLocalServiceProvider implements TaskLocalService {

    private final SharedPreferences sharedPreferences;
    private Collection<Task> localTasks;

    public TaskLocalServiceProvider(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        localTasks = new Gson()
                .fromJson(
                        sharedPreferences.getString("tasks", "[]"),
                        new TypeToken<List<Task>>(){}.getType()
                );
    }

    @Override
    public void save(Task task) {
        ArrayList<Task> tasks = new ArrayList<>(localTasks);
        task.setId(getNextId());
        tasks.add(task);

        saveTasks(tasks);
    }

    @Override
    public void update(Task task) {
        ArrayList<Task> tasks = new ArrayList<>(localTasks);
        for (Task oldTask: tasks){
            if (oldTask.getId() == task.getId()) {
                oldTask.setDescription(task.getDescription());
                break;
            }
        }

        saveTasks(tasks);
    }

    @Override
    public void remove(int taskId) {
        ArrayList<Task> tasks = new ArrayList<>(localTasks);
        ArrayList<Task> updatedTasks = new ArrayList<>(localTasks);
        for (int i = 0; i < tasks.size(); i++) {
            Task taskToDelete = tasks.get(i);
            if (taskToDelete.getId() == taskId) {
                updatedTasks.remove(i);
                break;
            }
        }

        saveTasks(updatedTasks);
    }

    private void saveTasks(List<Task> tasks) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("tasks", new Gson().toJson(tasks));

        editor.apply();

        localTasks = tasks;
    }

    @Override
    public List<Task> findAll() {
        return new ArrayList<>(localTasks);
    }

    private Integer getNextId() {
        return localTasks.size() + 1;
    }

    public static class Utils {
        public static TaskLocalService create() {
            SharedPreferences sharedPreferences =
                    TasksApplication.getSharedPrefs();
            return new TaskLocalServiceProvider(sharedPreferences);
        }
    }
}
