package net.decenternet.technicalexam.ui.tasks;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import net.decenternet.technicalexam.R;
import net.decenternet.technicalexam.domain.Task;

import java.util.List;

public class TasksListAdapter extends ListAdapter<Task, TasksListAdapter.ViewHolder> {

    private final OnClickBtnUpdateTaskListener onClickBtnUpdateTaskListener;
    private final OnCBTaskCheckChangeListener onCBTaskCheckChangeListener;

    public TasksListAdapter(
            OnClickBtnUpdateTaskListener onClickBtnUpdateTaskListener,
            OnCBTaskCheckChangeListener onCBTaskCheckChangeListener) {
        super(DIFF_CALLBACK);
        this.onClickBtnUpdateTaskListener = onClickBtnUpdateTaskListener;
        this.onCBTaskCheckChangeListener = onCBTaskCheckChangeListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_task,
                        parent, false);
        final ViewHolder viewHolder = new ViewHolder(itemView);
        viewHolder.btnUpdateTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickBtnUpdateTaskListener != null) {
                    int position = viewHolder.getAbsoluteAdapterPosition();
                    Task task = getItem(position);
                    onClickBtnUpdateTaskListener.onClickBtnUpdateTask(task);
                }
            }
        });

        viewHolder.getCbTask().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (onCBTaskCheckChangeListener != null){
                    int position = viewHolder.getAbsoluteAdapterPosition();
                    Task task = getItem(position);
                    onCBTaskCheckChangeListener.onCBTaskCheckChangeListener(task.getId(), isChecked);
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TasksListAdapter.ViewHolder holder, int position) {
        holder.bindData(getItem(position));
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final CheckBox cbTask;
        private final Button btnUpdateTask;

        public ViewHolder(View itemView) {
            super(itemView);

            cbTask = (CheckBox) itemView.findViewById(R.id.cb_task);
            btnUpdateTask = (Button) itemView.findViewById(R.id.btn_update_task);

        }

        public void bindData(Task data) {
            if (data.getDescription()!= null && !data.getDescription().isEmpty()){
                cbTask.setText(data.getDescription());
            }
        }

        public CheckBox getCbTask() {return  cbTask; }

        public Button getBtnUpdateTask() {
            return btnUpdateTask;
        }
    }

    public static final DiffUtil.ItemCallback<Task> DIFF_CALLBACK = new DiffUtil.ItemCallback<Task>() {

        @Override
        public boolean areItemsTheSame(@NonNull Task oldItem, @NonNull Task newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Task oldItem, @NonNull Task newItem) {
            return oldItem.equals(newItem);
        }
    };

    public interface OnClickBtnUpdateTaskListener {
        public void onClickBtnUpdateTask(Task task);
    }

    public interface OnCBTaskCheckChangeListener {
        public void onCBTaskCheckChangeListener(int taskId, boolean isChecked);
    }

}
