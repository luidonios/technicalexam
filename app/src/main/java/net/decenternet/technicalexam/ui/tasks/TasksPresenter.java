package net.decenternet.technicalexam.ui.tasks;

import net.decenternet.technicalexam.data.TaskLocalService;
import net.decenternet.technicalexam.data.TaskLocalServiceProvider;
import net.decenternet.technicalexam.domain.Task;

import java.util.ArrayList;
import java.util.List;

public class TasksPresenter implements TasksContract.Presenter {

    private final TaskLocalService taskLocalService;
    private final TasksContract.View view;
    private final List<Integer> selectedTaskIds = new ArrayList<>();

    public TasksPresenter(TasksContract.View view, TaskLocalService taskLocalService) {
        this.view = view;
        this.taskLocalService = taskLocalService;

        displayTasks();
    }

    @Override
    public void onAddTaskClicked() {

    }

    @Override
    public void onSaveTaskClicked(Task task) {
        taskLocalService.save(task);
        displayTasks();
    }

    private void displayTasks() {
        List<Task> tasks = taskLocalService.findAll();
        view.displayTasks(tasks);
    }

    @Override
    public void onUpdateTaskClicked(Task task) {
        taskLocalService.update(task);
        displayTasks();
    }

    @Override
    public void onTaskChecked(int taskId) {
        selectedTaskIds.add(taskId);
    }

    @Override
    public void onTaskUnchecked(int taskId) {
        if (selectedTaskIds.contains(taskId)) {
            int index = selectedTaskIds.indexOf(taskId);
            selectedTaskIds.remove(index);
        }
    }

    @Override
    public void onDeleteTaskClicked() {
        for(int taskId: selectedTaskIds) {
            taskLocalService.remove(taskId);
        }

        displayTasks();
    }

    public static class Utils {
        public static TasksPresenter create(TasksContract.View view) {
            TaskLocalService taskLocalService =
                    TaskLocalServiceProvider.Utils.create();
            return new TasksPresenter(view, taskLocalService);
        }
    }
}
