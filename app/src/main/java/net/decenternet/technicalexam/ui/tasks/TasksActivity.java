package net.decenternet.technicalexam.ui.tasks;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import net.decenternet.technicalexam.R;
import net.decenternet.technicalexam.domain.Task;
import net.decenternet.technicalexam.utils.AddTaskDialogUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TasksActivity extends AppCompatActivity
        implements TasksContract.View, TasksListAdapter.OnClickBtnUpdateTaskListener,
        TasksListAdapter.OnCBTaskCheckChangeListener {

    private TasksContract.Presenter presenter;
    private FloatingActionButton fabAddTask;
    private RecyclerView rvTasks;
    private TasksListAdapter tasksListAdapter;


    @Override
    protected void onDestroy() {
        super.onDestroy();

        tasksListAdapter = null;
        rvTasks = null;
        fabAddTask = null;
        presenter = null;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * Finish this simple task recording app.
         * The following are the remaining todos for this project:
         * 1. Make sure all defects are fixed.
         * 2. Showing a dialog to add/edit the task.
         * 3. Allowing the user to toggle it as completed.
         * 4. Allowing the user to delete a task.
         *
         */
        setContentView(R.layout.activity_tasks);

        setupRvTasks();
        setupFabTask();

        presenter = TasksPresenter.Utils.create(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.action_delete) {
            removeSelectedTasks();
        }
        return true;
    }

    private void removeSelectedTasks() {
        if (presenter!=null){
            presenter.onDeleteTaskClicked();
        }
    }

    private void setupFabTask() {
        fabAddTask = findViewById(R.id.fab_add_task);
        fabAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupTaskAddingDialog();
            }
        });
    }

    private void setupRvTasks() {
        rvTasks = findViewById(R.id.rv_tasks);
        tasksListAdapter = new TasksListAdapter(this,
                this);
        rvTasks.setAdapter(tasksListAdapter);

        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(this);
        rvTasks.setLayoutManager(linearLayoutManager);

        rvTasks.addItemDecoration(new DividerItemDecoration(this,
                linearLayoutManager.getOrientation()));
    }

    @Override
    public void displayTasks(List<Task> tasks) {

        if (tasksListAdapter != null) {
            //This is to fix the issue,
            //in ListAdapter not updating,
            //after calling submit list,
            // need more time to investigate
            tasksListAdapter.submitList(null);
            tasksListAdapter.submitList(tasks);
        }
    }

    @Override
    public void addTaskToList(Task task) {
        if (presenter != null) {
            presenter.onSaveTaskClicked(task);
        }
    }

    @Override
    public void removeTaskFromList(Task task) {

    }

    @Override
    public void updateTaskInList(Task task) {
        if (presenter != null) {
            presenter.onUpdateTaskClicked(task);
        }
    }

    @Override
    public void popupTaskAddingDialog() {

        AlertDialog alertDialog =
                AddTaskDialogUtils.create(this,
                        getString(R.string.alert_add_task_title),
                        getString(R.string.alert_positive_btn_add_text),
                        null,
                        new AddTaskDialogUtils.OnPositiveBtnClickListener() {
                            @Override
                            public void onPositiveBtnClick(String newTask) {
                                Task task = new Task();
                                task.setDescription(newTask);
                                addTaskToList(task);
                            }
                        });

        alertDialog.show();
    }

    @Override
    public void popupTaskEditorDialog(Task task) {
        final Task taskToUpdate = task;
        AlertDialog alertDialog =
                AddTaskDialogUtils.create(this,
                        getString(R.string.alert_update_task_title),
                        getString(R.string.alert_positive_btn_update_text),
                        taskToUpdate.getDescription(),
                        new AddTaskDialogUtils.OnPositiveBtnClickListener() {
                            @Override
                            public void onPositiveBtnClick(String newTask) {
                                if (!Objects.equals(newTask, taskToUpdate.getDescription())) {
                                    taskToUpdate.setDescription(newTask);
                                    updateTaskInList(taskToUpdate);
                                }
                            }
                        });

        alertDialog.show();
    }



    @Override
    public void onClickBtnUpdateTask(Task task) {
        popupTaskEditorDialog(task);
    }

    @Override
    public void onCBTaskCheckChangeListener(int taskId, boolean isChecked) {
        if (isChecked){
            presenter.onTaskChecked(taskId);
        } else {
            presenter.onTaskUnchecked(taskId);
        }
    }
}


