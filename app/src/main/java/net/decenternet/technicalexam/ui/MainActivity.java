package net.decenternet.technicalexam.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import android.widget.TextView;

import net.decenternet.technicalexam.R;
import net.decenternet.technicalexam.ui.tasks.TasksActivity;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity {

    private ImageView ivBrandingLogo;
    public static Boolean theCatalyst = true;

    private Handler handler = new Handler();
    private RedirectToTaskActivityRunnable redirectToTaskActivityRunnable =
            new RedirectToTaskActivityRunnable(this);
    //Max delayed before redirecting to TasksActivity
    private static final long MAX_DELAYED = 3000;

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(redirectToTaskActivityRunnable);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ivBrandingLogo = (ImageView) findViewById(R.id.ivBrandingLogo);
        /**
         * Tasks
         * 1. Change the text "Put your favorite quote here" with your own quote.
         * 2. Set any image that best illustrate the quote to ivBrandingLogo.
         * 3. Display this screen for 3 seconds, then redirect to TasksActivity and close this MainActivity.
         */
        handler.postDelayed(redirectToTaskActivityRunnable, MAX_DELAYED);
    }

    private static class RedirectToTaskActivityRunnable implements Runnable {

        private WeakReference<MainActivity> weakActivity;
        public RedirectToTaskActivityRunnable(MainActivity mainActivity) {
            weakActivity = new WeakReference<>(mainActivity);
        }

        @Override
        public void run() {
            MainActivity mainActivity = weakActivity.get();
            if (mainActivity != null) {
                //set theCatalyst to false,
                // to prevent throwing RuntimeException,
                //when redirecting to TAsksActivity
                MainActivity.theCatalyst = false;

                mainActivity.startActivity(new Intent(mainActivity, TasksActivity.class));
                mainActivity.finish();
            }
        }
    }

}