package net.decenternet.technicalexam.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import net.decenternet.technicalexam.R;

public class AddTaskDialogUtils {
    public static AlertDialog create(Context context,
                                     String alertMessage,
                                     String positiveBtnText,
                                     String taskDescription,
                                     final AddTaskDialogUtils.OnPositiveBtnClickListener onPositiveBtnClickListener) {

        AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(context);

        View view =
                LayoutInflater.from(context).inflate(R.layout.alert_add_task, null);
        final EditText etNewTask = (EditText) view.findViewById(R.id.et_new_task);
        if (taskDescription != null) {
            etNewTask.setText(taskDescription, TextView.BufferType.EDITABLE);
            etNewTask.setCursorVisible(true);
            etNewTask.setSelection(taskDescription.length());
        }
        alertDialogBuilder.setMessage(alertMessage)
                .setView(view)
                .setPositiveButton(positiveBtnText, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newTask = etNewTask.getText().toString();
                        if (onPositiveBtnClickListener != null) {
                            onPositiveBtnClickListener.onPositiveBtnClick(newTask);
                        }
                    }
                })
                .setNegativeButton(R.string.alert_negative_btn_text, null)
                .setCancelable(false);

        return alertDialogBuilder.create();
    }

    public interface OnPositiveBtnClickListener {
        public void onPositiveBtnClick(String newTask);
    }
}
